package io.hammok.schemad;

import io.hammok.schemad.components.api.APIOperation;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.openapi.RouterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
@SpringBootApplication
public class Main implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(Main.class);
    private Vertx vertx;
    private List<APIOperation> operations;

    public Main(@Autowired Vertx vertx, @Autowired List<APIOperation> operations) {
        this.vertx = vertx;
        this.operations = operations;
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }


    @Override
    public void run(String... args) {
        RouterBuilder.create(vertx, "src/main/resources/api/api.yaml")
                .onSuccess(routerBuilder -> {
                    log.info("hi success");
                    for (APIOperation op : operations) {
                        routerBuilder.operation(op.operationId()).handler(op);
                        log.info("Operation bound : {}", op.operationId());
                    }
                    HttpServer server = vertx.createHttpServer(
                            new HttpServerOptions().setPort(8080).setHost("localhost"));
                    server.requestHandler(routerBuilder.createRouter()).listen();
                })
                .onFailure(err -> {
                    log.error("aaaw, cant load API", err);
                });
    }
}
