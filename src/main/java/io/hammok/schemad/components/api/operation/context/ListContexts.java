package io.hammok.schemad.components.api.operation.context;

import io.hammok.schemad.components.api.APIOperation;
import io.hammok.schemad.services.schema.SchemaRepository;
import io.vertx.ext.web.RoutingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
@Component
public class ListContexts implements APIOperation {

    private SchemaRepository repo;

    public ListContexts(@Autowired SchemaRepository repo) {
        this.repo = repo;
    }

    @Override
    public String operationId() {
        return "listContexts";
    }

    @Override
    public void handle(RoutingContext routingContext) {
        routingContext.response().end(this.repo.listAll().encode());
    }
}
