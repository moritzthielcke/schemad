package io.hammok.schemad.components.api.operation.context;

import io.hammok.schemad.components.api.APIOperation;
import io.hammok.schemad.services.schema.SchemaRepository;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
@Component
public class CreateContext implements APIOperation{
    private static final Logger log = LoggerFactory.getLogger(CreateContext.class);
    private SchemaRepository repo;

    public CreateContext(@Autowired SchemaRepository repo) {
        this.repo = repo;
    }

    @Override
    public String operationId() {
        return "createContext";
    }

    @Override
    public void handle(RoutingContext routingContext) {
        JsonObject obj = routingContext.getBodyAsJson();
        obj = repo.create(obj);
        log.info("create new context : {}", obj);
        routingContext.response().end("hi from {}", operationId());
    }
}
