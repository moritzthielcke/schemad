package io.hammok.schemad.components.api.operation;

import io.hammok.schemad.components.api.APIOperation;
import io.vertx.ext.web.RoutingContext;
import org.springframework.stereotype.Component;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
@Component
public class ListSchemas implements APIOperation {
    @Override
    public String operationId() {
        return "listSchemas";
    }

    @Override
    public void handle(RoutingContext routingContext) {
        routingContext.response().end("hi from {}", operationId());
    }
}
