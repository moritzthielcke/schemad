package io.hammok.schemad.components.api;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
public interface APIOperation extends Handler<RoutingContext> {
    public String operationId();
}
