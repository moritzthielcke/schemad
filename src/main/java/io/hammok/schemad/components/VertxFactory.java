package io.hammok.schemad.components;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
@Component
public class VertxFactory {

    private Vertx vertx;

    public VertxFactory(){
        vertx = Vertx.vertx(new VertxOptions());
    }

    @Bean
    public Vertx vertx(){
        return vertx;
    }

}
