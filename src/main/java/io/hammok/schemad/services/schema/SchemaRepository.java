package io.hammok.schemad.services.schema;

import com.fasterxml.jackson.databind.util.JSONPObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
@Service
public class SchemaRepository {
    private static final Logger log = LoggerFactory.getLogger(SchemaRepository.class);
    private final Map<String, List<JsonObject>> storage;

    public SchemaRepository() {
        this.storage = new HashMap<>();
        List<JsonObject> testSchemaRecords = new ArrayList<>();
        testSchemaRecords.add(new JsonObject().put("_id", "test").put("_rev", 1) );
        this.storage.put("test", testSchemaRecords);
    }

    public JsonObject findLatestRev(String uuid) throws SchemaNotFoundException{
        List<JsonObject> document = storage.get(uuid);
        if(document != null && !document.isEmpty()) {
            return document.get(document.size() - 1);
        }
        throw new SchemaNotFoundException("cant find schema with id = "+uuid);
    }

    public JsonArray listAll(){
        JsonArray result = new JsonArray();
        this.storage.forEach((key, jsonObjects) -> {
            try {
                result.add(findLatestRev(key));
            } catch (SchemaNotFoundException e) {
                log.error("cant read schema for {}",key,e);
            }
        });
        return result;
    }

    /***
     * reads the latest rev of a given ID
     */
    public JsonObject create(JsonObject object){
        List<JsonObject> revisions = new ArrayList<>();
        revisions.add(object);
        String id = UUID.randomUUID().toString();
        object.put("_id", id);
        object.put("_rev", 1);
        storage.put(id, revisions);
        return object;
    }

}
