package io.hammok.schemad.services.schema;

/**
 * Author:      moritz thielcke
 * Date:        24.06.2021
 * Description:
 */
public class SchemaNotFoundException extends Exception{

    public SchemaNotFoundException(String message) {
        super(message);
    }

    public SchemaNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
